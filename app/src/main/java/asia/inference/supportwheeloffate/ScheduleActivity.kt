package asia.inference.supportwheeloffate

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import asia.inference.supportwheeloffate.Model.Engineer
import asia.inference.supportwheeloffate.Model.WorkDay
import asia.inference.supportwheeloffate.Model.WorkShift
import asia.inference.supportwheeloffate.adapter.TimeTableAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.activity_schedule.toolbar
import java.util.*
import java.util.Calendar.*
import kotlin.collections.ArrayList


class ScheduleActivity : AppCompatActivity() {
    var gson = Gson()
    var numberOfDays = 10
    var numberOfShift = 2
    var mNextMonday: Date? = null
    var workingDays: ArrayList<WorkDay> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(R.color.dark))
        supportActionBar!!.title = "Schedule"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
//        supportActionBar!!.setDisplayShowTitleEnabled(false)

        var engineers = intent.extras!!.getParcelableArrayList<Engineer>("engineers") as ArrayList<Engineer>
        Log.d("TAG",gson.toJson(engineers))
        recycler_view.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager
        recycler_view.setItemAnimator(DefaultItemAnimator())
        recycler_view.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))

        workingDays = getNextWorkingDays()

        workingDays.mapIndexed { index, workDay ->
            var arrayShift: ArrayList<WorkShift> = ArrayList()

            engineers.shuffled().map {
                var currentEngineer = it
                if (index > 0){
                    if(workingDays[index-1].shift.filter { shift -> shift.engineer!!._id == currentEngineer._id}.isNullOrEmpty()){
                        if(arrayShift.size < 2 && it.numberOfShift < numberOfShift){
                            arrayShift.add(WorkShift(it))
                            ++it.numberOfShift
                        }
                    }
                } else {
                    if(arrayShift.size < 2 && it.numberOfShift < numberOfShift){
                        arrayShift.add(WorkShift(it))
                        ++it.numberOfShift
                    }
                }
            }
            workDay.shift = arrayShift
        }
        Log.d("TAG", "workingDays: ${gson.toJson(workingDays)}")

        val adapter = TimeTableAdapter(this, workingDays)
        recycler_view.setAdapter(adapter)
    }

    private fun getNextMonday(): Date {
        val date = getInstance()
        var diff = Calendar.MONDAY - date.get(Calendar.DAY_OF_WEEK)
        if (diff <= 0) {
            diff += 7
        }
        date.add(Calendar.DAY_OF_MONTH, diff)
        System.out.println(date.getTime())
        return date.time
    }

    private fun getNextWorkingDays(): ArrayList<WorkDay> {
        var weekdays: ArrayList<WorkDay> = ArrayList()
        val cal = Calendar.getInstance()
        cal.time = getNextMonday()
        while (weekdays.size < numberOfDays){
            val dayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
            Log.d("TAG", "${dayOfWeek == Calendar.SATURDAY}")
            if (dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY)

                weekdays.add(WorkDay(cal.getTime()))
            cal.add(Calendar.DAY_OF_MONTH, 1)
        }
        Log.d("TAG", gson.toJson(weekdays))
        return weekdays
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return super.onOptionsItemSelected(item)
    }

}
