package asia.inference.supportwheeloffate.helper

import android.content.Context
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

private var mInstance: SingletonRequestQueue? = null
private var mContext: Context? = null
private var mRequestQueue: RequestQueue? = null

class SingletonRequestQueue(context: Context){
    init {
        mContext = context
        mRequestQueue = getRequestQueue()
    }
//    private fun SingletonRequestQueue(context: Context): SingletonRequestQueue? {
//        mContext = context
//        mRequestQueue = getRequestQueue()
//    }

    @Synchronized
    fun getInstance(context: Context): SingletonRequestQueue {
        if (mInstance == null) {
            mInstance =
                SingletonRequestQueue(context)
        }
        return mInstance!!
    }

    fun getRequestQueue(): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext)
        }
        return mRequestQueue
    }
}
