package asia.inference.supportwheeloffate.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import asia.inference.supportwheeloffate.Model.WorkDay
import asia.inference.supportwheeloffate.R
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.item_recyclerview_timetable.view.*
import androidx.recyclerview.widget.GridLayoutManager
import java.text.SimpleDateFormat
import java.text.ParseException
import java.util.*
import kotlin.collections.ArrayList


class TimeTableAdapter(private val context: Context, private val itemList: ArrayList<WorkDay>) :
    RecyclerView.Adapter<TimeTableAdapter.CustomRecyclerView>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomRecyclerView {
        val layoutView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_timetable, null)
        return CustomRecyclerView(layoutView)
    }
    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: CustomRecyclerView, position: Int) {
        val myData = itemList[position]
        val EEEEFormat = SimpleDateFormat("EE, dd MMMM YYYY")
        holder.txtLabel.text = EEEEFormat.format(myData.date)
        val shiftArray = myData.shift
        val adapter = ShiftHorizontalAdapter(context, shiftArray)
        holder.recyclerView.setHasFixedSize(true)
//        holder.recyclerView.setLayoutManager(
//            LinearLayoutManager(
//                context,
//                LinearLayoutManager.HORIZONTAL,
//                false
//            )
//        )
        holder.recyclerView.layoutManager = GridLayoutManager(context, 2)
        holder.recyclerView.setAdapter(adapter)
        adapter.notifyDataSetChanged()

    }
    fun parseDate(
        inputDateString: String,
        inputDateFormat: SimpleDateFormat,
        outputDateFormat: SimpleDateFormat
    ): String? {
        var date: Date? = null
        var outputDateString: String? = null
        try {
            date = inputDateFormat.parse(inputDateString)
            outputDateString = outputDateFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return outputDateString
    }

    inner class CustomRecyclerView internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        internal var txtLabel: TextView = itemView.findViewById(R.id.txtLabel)
        internal var recyclerView = itemView.shift_recyclerview
    }
}