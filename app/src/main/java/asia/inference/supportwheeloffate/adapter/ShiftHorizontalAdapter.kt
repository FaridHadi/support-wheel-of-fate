package asia.inference.supportwheeloffate.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import asia.inference.supportwheeloffate.Model.WorkShift
import asia.inference.supportwheeloffate.R
import kotlinx.android.synthetic.main.content_schedule.view.*
import kotlinx.android.synthetic.main.item_recyclerview_shift.view.*

class ShiftHorizontalAdapter(private val mContext: Context?, private val shiftArray: ArrayList<WorkShift>) : RecyclerView.Adapter<ShiftHorizontalAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShiftHorizontalAdapter.ViewHolder {
        val layoutView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_recyclerview_shift, null)
        return ViewHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return shiftArray.size
    }

    override fun onBindViewHolder(holder: ShiftHorizontalAdapter.ViewHolder, position: Int) {
        var data = shiftArray[position]
        holder.txtLabel.text = data.engineer!!.name
        holder.txtNumber.text = "Shift Number : ${position + 1}"
    }

    inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        internal var txtLabel = itemView.txtLabel
        internal var txtNumber = itemView.txtShiftNumber
    }

}
