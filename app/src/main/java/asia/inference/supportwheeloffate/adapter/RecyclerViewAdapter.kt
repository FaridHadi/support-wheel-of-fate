package asia.inference.supportwheeloffate.adapter

import android.content.Context
import android.graphics.Bitmap
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import asia.inference.supportwheeloffate.Model.Engineer
import asia.inference.supportwheeloffate.R
import asia.inference.supportwheeloffate.helper.SingletonRequestQueue
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader

class RecyclerViewAdapter(context: Context, private val itemList: ArrayList<Engineer>) :
    RecyclerView.Adapter<RecyclerViewAdapter.CustomRecyclerView>() {
    override fun getItemCount(): Int {
        return this.itemList.size
    }

    private val mRequestQueue: RequestQueue
    private val mImageLoader: ImageLoader

//    val itemCount: Int
//        get() = this.itemList.size


    init {
        mRequestQueue = SingletonRequestQueue(context).getRequestQueue()!!
        mImageLoader = ImageLoader(mRequestQueue, object : ImageLoader.ImageCache {
            private val mCache = LruCache<String, Bitmap>(10)

            override fun putBitmap(url: String, bitmap: Bitmap) {
                mCache.put(url, bitmap)
            }

            override fun getBitmap(url: String): Bitmap {
                return mCache.get(url)
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomRecyclerView {
        val layoutView =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_row, null)
        return CustomRecyclerView(layoutView)
    }

    override fun onBindViewHolder(holder: CustomRecyclerView, position: Int) {
        val myData = itemList[position]
        holder.txtLabel.text = "${position + 1}. "+ myData.name
    }

    inner class CustomRecyclerView internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        internal var txtLabel: TextView
//        internal var avatar: NetworkImageView

        init {
            txtLabel = itemView.findViewById(R.id.txtLabel)
//            avatar = itemView.findViewById(R.id.imgNetwork)
        }
    }
}