package asia.inference.supportwheeloffate

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import asia.inference.supportwheeloffate.Model.Engineer
import asia.inference.supportwheeloffate.adapter.RecyclerViewAdapter
import asia.inference.supportwheeloffate.helper.SingletonRequestQueue
import com.android.volley.*
import com.android.volley.toolbox.JsonArrayRequest
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var queue: RequestQueue? = null
    private var engineers: ArrayList<Engineer>? = null
    val gson = Gson()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(R.color.dark))
        supportActionBar!!.title = "Engineers"
        queue = SingletonRequestQueue(this.applicationContext)
            .getRequestQueue()
        recycler_view.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager
        recycler_view.setItemAnimator(DefaultItemAnimator())
        recycler_view.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        getEngineers()

        fab.setOnClickListener {
            engineers?.let {
//                Toast.makeText(this, gson.toJson(it), Toast.LENGTH_LONG).show()
                startActivity(
                    Intent(
                        this@MainActivity,
                        ScheduleActivity::class.java
                    ).putParcelableArrayListExtra("engineers", it)
                )
            } ?: run {
                // If b is null.
                Toast.makeText(this, "No Engineers available", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getEngineers(){
        val jsonObjectRequest = object :
            JsonArrayRequest(
                Method.GET,"http://airasia-api.faredhadi.com/engineers", null,
                Response.Listener { response ->
                    engineers = gson.fromJson(response.toString(), genericType<ArrayList<Engineer>>())
                    Log.e("TAG", "RESPONSE: ${gson.toJson(engineers)}")
                    val adapter = RecyclerViewAdapter(
                        this,
                        engineers!!
                    )
                    recycler_view.setAdapter(adapter)
                }, errorListener
            ) {

            override fun getBodyContentType(): String {
                return "application/json"
            }

            override fun getPriority(): Priority {
                return Priority.IMMEDIATE
            }
        }

        queue!!.add(jsonObjectRequest)
    }

    internal var errorListener: Response.ErrorListener =
        Response.ErrorListener { error ->
            if (error is NetworkError) {
                Toast.makeText(applicationContext, "No network available", Toast.LENGTH_LONG).show()
                Log.d("TAG", "Error ${error.message}")
            } else {
                Toast.makeText(applicationContext, error.toString(), Toast.LENGTH_LONG).show()
                Log.d("TAG", "Error $error")
            }
        }
}
