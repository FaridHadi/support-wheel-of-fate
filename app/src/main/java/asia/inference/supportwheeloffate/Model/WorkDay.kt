package asia.inference.supportwheeloffate.Model

import java.util.*
import kotlin.collections.ArrayList

class WorkDay(var date: Date? = null,
              var shift: ArrayList<WorkShift> = ArrayList()
)