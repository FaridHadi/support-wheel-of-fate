package asia.inference.supportwheeloffate.Model

import android.os.Parcel
import android.os.Parcelable

class Engineer(var _id: String? = null,
               var name: String? = null,
               var numberOfShift: Int = 0): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(_id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Engineer> {
        override fun createFromParcel(parcel: Parcel): Engineer {
            return Engineer(parcel)
        }

        override fun newArray(size: Int): Array<Engineer?> {
            return arrayOfNulls(size)
        }
    }
}